<?php 
require_once "connection_pdo.php";
require_once "util.php";
session_start();

if (isset($_REQUEST['cancel'])) {
    header("Location: login.php");
    return;
}

if (isset($_REQUEST['done'])) {
    try {
        $login = htmlentities($_REQUEST['login']);
        $password = htmlentities($_REQUEST['password']);
    
        if (!emptyCheck($login)) throw new UserAuthException("Missing login");
        elseif (!emptyCheck($password)) throw new UserAuthException("Missing password");
    
        $salt = "Ab^%ChlOOZXC*&";
        $password = hash('md5', $salt.$password);
    
        $stmt = $pdo->prepare("SELECT user_id, password FROM users WHERE login = :lg");
        $stmt->execute(array(":lg" => $login));
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
    
        if ($result === false) throw new UserAuthException("Incorrect login");
        elseif ($result['password'] != $password) throw new UserAuthException("Incorrect password");
        else {
            $_SESSION['user_id'] = $result['user_id'];
            $_SESSION['user_login'] = $login;
            header("Location: projects_view.php?user_id=".$_SESSION['user_id']);
            return;
        }
    } catch (IUserAuthException $e) {
        $_SESSION['error'] = $e->getMessage();
        header("Location: login.php");
        return;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>Login</title>
</head>
<body>
    <div id="wrapper">
        <div class="form_container">
            <h2 class="left">Log in</h2>
            <a id="sign_up" href="sign_up.php">Sign up</a>
            <?php 
                if (isset($_SESSION['error'])) flashError(htmlentities($_SESSION['error']));
                if (isset($_SESSION['success'])) flashSuccess(htmlentities($_SESSION['success']));
            ?>
            <form action="">
                <label for="user_login">Enter your login</label><input type="text" name="login" id="user_login" placeholder="Your login...">
                <label for="user_password">Enter your password</label><input type="password" name="password" id="user_password" placeholder="Your password...">
                <input type="submit" class="btn_done" name="done" value="Log in">
                <input type="submit" class="btn_cancel" name="cancel" value="Cancel">
            </form>
        </div>
    </div>
</body>
</html>
<?php 
require_once "connection_pdo.php";
require_once "util.php";
session_start();

try {
    if (!isset($_SESSION['user_id'])) throw new UserAuthException("You must login first");
    
    if (!isset($_REQUEST['project_id'])) throw new UserInsertDataException("Missing project id");
    
    if (!isset($_REQUEST['task_id'])) throw new UserInsertDataException("Missing task id");
    
    $access_check = $pdo->prepare("SELECT task_name FROM users INNER JOIN projects ON users.user_id = projects.user_id 
    INNER JOIN tasks ON projects.project_id = tasks.project_id 
    WHERE users.user_id = :ui AND projects.project_id = :pri AND task_id = :ti");
    $access_check->execute(array(":ui" => htmlentities($_SESSION['user_id']), ":pri" => htmlentities($_REQUEST['project_id']), ":ti" => htmlentities($_REQUEST['task_id'])));
    
    if ($access_check->rowCount() === 0) throw new UserInsertDataException("You do not have access to delete this task");
    else {
        $stmt = $pdo->prepare("DELETE FROM tasks WHERE task_id = :ti");
        $stmt->execute(array(":ti" => htmlentities($_REQUEST['task_id'])));
    
        $_SESSION['success'] = "Task successfully deleted";
        header("Location: projects_view.php?user_id=".$_SESSION['user_id']);
        return;
    }
} catch (IUserAuthException $e) {
    $_SESSION['error'] = $e->getMessage();
    header("Location: login.php");
    return;
} catch (IUserInsertDataException $e) {
    $_SESSION['error'] = $e->getMessage();
    header("Location: projects_view.php?user_id=".$_SESSION['user_id']);
    return;
}
?>
<?php 
require_once "connection_pdo.php";
require_once "util.php";
session_start();

try {
    if (!isset($_SESSION['user_id'])) throw new UserAuthException("You must login first");
    
    if (!isset($_REQUEST['project_id'])) throw new UserInsertDataException("Missing project id");

    $project_id = htmlentities($_REQUEST['project_id']);
    $stmt = $pdo->prepare("SELECT project_name FROM projects WHERE user_id = :ui AND project_id = :pri");
    $stmt->execute(array(":ui" => htmlentities($_SESSION['user_id']), ":pri" => htmlentities($project_id)));
    $proj_info = $stmt->fetch(PDO::FETCH_ASSOC);
    
    if ($proj_info === false) throw new UserInsertDataException("Incorrect project id");

    $task_name = htmlentities($_REQUEST['task_name']);
    $priority = htmlentities($_REQUEST['priority']);
    $deadline = htmlentities($_REQUEST['deadline']);
    $status = htmlentities($_REQUEST['status']);

    if (!emptyCheck($task_name) || !emptyCheck($priority) || !emptyCheck($deadline)) throw new UserInsertDataException("All fields required");

    $stmt = $pdo->prepare("INSERT INTO tasks (project_id, task_name, priority, deadline, status) 
    VALUES (:proj_id, :tn, :pri, :dl, :st)");
    $stmt->execute(array(":proj_id" => htmlentities($_REQUEST['project_id']), ":tn" => $task_name, ":pri" => $priority, 
    ":dl" => $deadline, ":st" => $status));

    $_SESSION['success'] = "Task successfully created";
    return;
} catch (IUserAuthException $e) {
    $_SESSION['error'] = $e->getMessage();
    header("Location: login.php");
    return;
} catch (IUserInsertDataException $e) {
    $_SESSION['error'] = $e->getMessage();
    header("Location: projects_view.php?user_id=".$_SESSION['user_id']);
    return;
}
?>

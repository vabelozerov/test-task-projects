<?php 
require_once "connection_pdo.php";
require_once "util.php";
session_start();

if (isset($_REQUEST['cancel'])) {
    header("Location: login.php");
    return;
}

try {
    if (isset($_REQUEST['sign_up'])) {
        $login = htmlentities($_REQUEST['login']);
        $email = htmlentities($_REQUEST['email']);
        $password = htmlentities($_REQUEST['password']);
    
        if (!emptyCheck($login)) throw new UserInsertDataException("Missing login"); 
        elseif (!emptyCheck($email)) throw new UserInsertDataException("Missing email");
        elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) throw new UserInsertDataException("Incorrect email");
        elseif (!emptyCheck($password)) throw new UserInsertDataException("Missing password");
    
        $stmt = $pdo->prepare("SELECT login FROM users WHERE login = :lg");
        $stmt->execute(array(":lg" => $login));
        if ($stmt->rowCount() !== 0) throw new UserInsertDataException("This login is already in use!");
    
        $stmt = $pdo->prepare("SELECT email FROM users WHERE email = :em");
        $stmt->execute(array(":em" => $email));
        if ($stmt->rowCount() !== 0) throw new UserInsertDataException("This email is already in use!");
    
        $salt = "Ab^%ChlOOZXC*&";
        $password = hash('md5', $salt.$password);
    
        $stmt = $pdo->prepare("INSERT INTO users (login, password, email) VALUES (:lg, :pas, :em)");
        $stmt->execute(array(":lg" => $login, ":pas" => $password, ":em" => $email));
        $_SESSION['success'] = "Registration successful!";
        header("Location: login.php");
        return;
    }
} catch (IUserInsertDataException $e) {
    $_SESSION['error'] = $e->getMessage();
    header("Location: sign_up.php");
    return;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>Sign up</title>
</head>
<body>
    <div id="wrapper">
        <div class="form_container">
            <h2 class="center">Sign up</h2>
            <?php if (isset($_SESSION['error'])) flashError(htmlentities($_SESSION['error'])); ?>
            <form action="">
                <label for="user_login">Enter your login</label><input type="text" name="login" id="user_login" placeholder="Your login...">
                <label for="user_email">Enter your email</label><input type="email" name="email" id="user_email" placeholder="Your email...">
                <label for="user_password">Enter your password</label><input type="password" name="password" id="user_password" placeholder="Your password...">                <input type="submit" class="btn_done" name="sign_up" value="Sign Up">
                <input type="submit" class="btn_cancel" name="cancel" value="Cancel">
            </form>
        </div>
    </div>
</body>
</html>
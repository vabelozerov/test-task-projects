<?php 
require_once "connection_pdo.php";
require_once "util.php";
session_start();

try {
    if (!isset($_REQUEST['user_id'])) throw new UserAuthException("Missing users id");
    
    if (!isset($_SESSION['user_id'])) throw new UserAuthException("Please log in");
    
    if ($_REQUEST['user_id'] !== $_SESSION['user_id']) throw new UserAuthException("You do not have access to this page");
    
    if (isset($_REQUEST['cancel'])) {
        header("Location: projects_view.php?user_id=".$_SESSION['user_id']);
        return;
    }
    
    if (isset($_REQUEST['add'])) {
        $project_name = htmlentities($_REQUEST['project_name']);
    
        if (!emptyCheck($project_name)) throw new UserInsertDataException("Missing project name");
    
        $stmt = $pdo->prepare("INSERT INTO projects (user_id, project_name) VALUES (:ui, :pn)");
        $stmt->execute(array(":ui" => htmlentities($_SESSION['user_id']), ":pn" => $project_name));
    
        $_SESSION['success'] = "Project successfully created";
        header("Location: projects_view.php?user_id=".$_SESSION['user_id']);
        return;
    }
    
    $user_id = htmlentities($_SESSION['user_id']);
    $proj_list = $pdo->prepare("SELECT project_id, project_name FROM projects WHERE user_id = :ui");
    $proj_list->execute(array(":ui" => $user_id));
} catch (IUserAuthException $e) {
    $_SESSION['error'] = $e->getMessage();
    header("Location: login.php");
    return;
} catch (IUserInsertDataException $e) {
    $_SESSION['error'] = $e->getMessage();
    header("Location: projects_view.php?user_id=".$_SESSION['user_id']);
    return;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>
    <title>User's Projects</title>
    <script>
        $(document).ready(function() {
            $(".task_update_form").submit(function() {
                var input_data = {};
                $(this).find('input[type="hidden"], input[type="text"]').each(function() {
                    input_data[this.name] = $(this).val();
                });

                if ($(this).find('input[type="checkbox"]').is(":checked")) input_data["status"] = 1;
                else input_data["status"] = "";

                $.ajax({
                    url: "update_task.php",
                    method: "post",
                    data: input_data
                });
            });

            $(".task_add_form").submit(function() {
                var input_data = {};
                $(this).find('input[type="hidden"], input[type="text"]').each(function() {
                    input_data[this.name] = $(this).val();
                });

                if ($(this).find('input[type="checkbox"]').is(":checked")) input_data["status"] = 1;
                else input_data["status"] = "";

                $.ajax({
                    url: "add_task.php",
                    method: "post",
                    data: input_data
                });
            });
        });
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="project_info">
            <h2>Welcome, <?= $_SESSION['user_login'] ?>!</h2>
            <div id="logout">
                <a href="logout.php">Logout</a>
            </div>
            <h2>Your projects</h2>
            <div id="project_flash">
                <?php 
                    if (isset($_SESSION['error'])) {
                        echo '<span class="center">';
                        flashError(htmlentities($_SESSION['error']));
                        echo '</span>';
                    }
                    if (isset($_SESSION['success'])) {
                        echo '<span class="center">';
                        flashSuccess(htmlentities($_SESSION['success']));
                        echo '</span>';
                    }
                ?>
            </div>
            <?php
                if ($proj_list->rowCount() === 0) echo '<h2>You have no projects yet. Want to create a new one?</h2>';

                while ($project = $proj_list->fetch(PDO::FETCH_ASSOC)) {
                    echo '<div class="project_wrapper"><p class="project_name">'.htmlentities($project['project_name'])."</p>";
                    echo '<div class="project_btn"><a href="update_project.php?project_id='.htmlentities($project['project_id']).'">Update</a></div>';
                    echo '<div class="project_btn"><a href="delete_project.php?project_id='.htmlentities($project['project_id']).'">Delete</a></div>';
                    $tasks_list = $pdo->prepare("SELECT task_id, task_name, priority, deadline, status FROM tasks WHERE project_id = :pri");
                    $tasks_list->execute(array(":pri" => htmlentities($project['project_id'])));
                    echo '<div id="project_info">';
                    while($task = $tasks_list->fetch(PDO::FETCH_ASSOC)) {
                        if ($task['status']) echo '<div class="form_container">';
                        else echo '<div class="form_container">';

                        echo '<form method="post" class="task_update_form">';
                        echo '<h2 class="center">Task Info</h2>';
                        echo '<input type="hidden" name="project_id" value="'.htmlentities($project['project_id']).'">';
                        echo '<input type="hidden" name="task_id" value="'.htmlentities($task['task_id']).'">';
                        echo '<label>Task Name</label><input type="text" name="task_name" value="'.htmlentities($task['task_name']).'" placeholder="Task name...">';
                        echo '<label>Priority</label><input type="text" name="priority" value="'.htmlentities($task['priority']).'" placeholder="Priority...">';
                        echo '<label>Deadline</label><input type="text" name="deadline" value="'.htmlentities($task['deadline']).'" placeholder="Deadline...">';
                        echo '<label class="done_lbl left">Done</label><input type="checkbox" name="status"';
                        if ($task['status']) echo "checked>";
                        else echo ">";
                        echo '<input type="submit" name="task_update" class="task_btn" id="task_update_btn" value="Save">';
                        echo '<span class="task_btn delete_btn"><a href="delete_task.php?project_id='.htmlentities($project['project_id']).'&task_id='.htmlentities($task['task_id']).'">Delete</a></span>';
                        echo "</form></div>";
                    }

                    echo '<div class="form_container">';
                    echo '<h2 class="center">Add task</h2>';
                    echo '<form method="post" class="task_add_form">';
                    echo '<input type="hidden" name="project_id" value="'.htmlentities($project['project_id']).'">';
                    echo '<label>Task Name</label><input type="text" name="task_name" placeholder="Task name...">';
                    echo '<label>Priority</label><input type="text" name="priority" placeholder="Priority...">';
                    echo '<label>Deadline</label><input type="text" name="deadline" placeholder="Deadline...">';
                    echo '<label class="done_lbl left">Done</label><input type="checkbox" name="status">';
                    echo '<input type="submit" class="task_btn add_btn" name="task_add" id="task_add_btn" value="Add">';
                    echo "</form></div>";
                    echo "</div>";
                    echo '</div>';
                }
            ?>
        </div>
        <div class="form_container">
            <h2 class="center">Create new project</h2>
            <form action="">
                <input type="hidden" name="user_id" value="<?= htmlentities($_SESSION['user_id']) ?>">
                <label for="project_name">Project name</label><input type="text" id="project_name" name="project_name" id="proj_name" placeholder="Project name...">
                <input type="submit" class="btn_done" name="add" value="Create">
                <input type="submit" class="btn_cancel" name="cancel" value="Cancel">
            </form>   
        </div>
    </div>
</body>
</html>
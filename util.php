<?php 
function emptyCheck($data) { return strlen(trim($data)) > 0 ? true : false; }

function flashError($error) {
    echo '<p class="error">'.htmlentities($error)."</p>";
    unset($_SESSION['error']);
}

function flashSuccess($success) {
    echo '<p class="success">'.htmlentities($success).'</p>';
    unset($_SESSION['success']);
}

interface IException {}
  interface IUserException extends IException {}
    interface IUserAuthException extends IUserException {}
    interface IUserInsertDataException extends IUserException {}
    interface IUserUpdateDataException extends IUserException {}

class UserAuthException extends Exception implements IUserAuthException {}
class UserInsertDataException extends Exception implements IUserInsertDataException {}
class UserUpdateDataException extends Exception implements IUserUpdateDataException {}
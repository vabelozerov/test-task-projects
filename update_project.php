<?php 
require_once "connection_pdo.php";
require_once "util.php";
session_start();

try {
    if (!isset($_SESSION['user_id'])) throw new UserAuthException("You must login first");
    
    if (!isset($_REQUEST['project_id'])) throw new UserInsertDataException("Missing project id");
    
    if (isset($_REQUEST['cancel'])) {
        header("Location: projects_view.php?user_id=".$_SESSION['user_id']);
        return;
    }
    
    if (isset($_REQUEST['update_project'])) {
        $project_name = htmlentities($_REQUEST['project_name']);
    
        if (!emptyCheck($project_name)) throw new UserUpdateDataException("Missing project name");
    
        $stmt = $pdo->prepare("UPDATE projects SET project_name = :pn WHERE project_id = :pri");
        $stmt->execute(array(":pn" => $project_name, ":pri" => htmlentities($_REQUEST['project_id'])));   
    
        $_SESSION['success'] = "Project successfully updated";
        header("Location: projects_view.php?user_id=".$_SESSION['user_id']);
        return;
    }
    
    $project_id = htmlentities($_REQUEST['project_id']);
    $stmt = $pdo->prepare("SELECT project_name FROM projects WHERE user_id = :ui AND project_id = :pri");
    $stmt->execute(array(":ui" => htmlentities($_SESSION['user_id']), ":pri" => $project_id));
    $proj_info = $stmt->fetch(PDO::FETCH_ASSOC);
    
    if ($proj_info === false) throw new UserInsertDataException("Incorrect project id");
} catch (IUserAuthException $e) {
    $_SESSION['error'] = $e->getMessage();
    header("Location: login.php");
    return;
} catch (IUserInsertDataException $e) {
    $_SESSION['error'] = $e->getMessage();
    header("Location: projects_view.php?user_id=".$_SESSION['user_id']);
    return;
} catch (IUserUpdateDataException $e) {
    $_SESSION['error'] = $e->getMessage();
    header("Location: update_project.php?project_id=".$_REQUEST['project_id']);
    return;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>Update project</title>
</head>
<body>
    <div id="wrapper">
        <div class="form_container">
            <h2 class="center">Update project info</h2>
            <?php if (isset($_SESSION['error'])) flashError($_SESSION['error']); ?>
            <form action="">
                <label for="project_name">Enter project name</label><input type="hidden" id="project_name" name="project_id" value=<?= htmlentities($project_id) ?>>
                <input type="text" id="proj_name" name="project_name" value="<?= htmlentities($proj_info['project_name']) ?>" placeholder="Project name...">
                <input type="submit" class="btn_done" name="update_project" value="Update">
                <input type="submit" class="btn_cancel" name="cancel" value="Cancel">
            </form>
        </div>
    </div>
</body>
</html>